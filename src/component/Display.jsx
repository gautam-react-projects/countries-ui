import Data from "./countries.json";
import Card from "./Card";
import React, { useState, useEffect } from "react";
import Regioncard from "./Regioncard";

export default function Display() {
  const [Screen, setScreen] = useState([]);
  const [Region, setRegion] = useState("");
  const [SubRegion, setSubRegion] = useState("");
  const [SearchCountry, setSearchCountry] = useState("");

  function handleClick(event) {
    const newregion = event.target.value;

    setRegion(newregion);

    const regionalCountries = Data.filter((obj) => {
      if (obj.region === newregion) {
        return obj;
      }
    }).map((obj) => {
      return <Card data={obj} />;
    });

    setScreen(regionalCountries);
  }

  function handleClick1(event) {
    const SUBREGION = event.target.value;

    setSubRegion(SUBREGION);

    if (Region === "") {
      const subregionalCountries = Data.filter((obj) => {
        if (obj.subregion === SUBREGION) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });

      setScreen(subregionalCountries);
    } else {
      const subregionalCountries = Data.filter((obj) => {
        if (obj.subregion === SUBREGION && obj.region === Region) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });

      setScreen(subregionalCountries);
    }
  }

  function handleClick2(event) {
    const sorting = event.target.value;

    console.log(sorting);

    let ted = Data;

    if (SearchCountry !== "") {
      ted = Data.filter((obj) => {
        let uppercaseCountry = obj.name.official.toUpperCase();
        let lowercaseCountry = obj.name.official.toLowerCase();
        if (
          (uppercaseCountry.includes(SearchCountry) ||
            lowercaseCountry.includes(SearchCountry)) &&
          Region === obj.region &&
          SubRegion === obj.subregion
        ) {
          return obj;
        }
      });
    }

    if (Region !== "") {
      ted = ted.filter((obj) => {
        if (obj.region === Region) {
          return obj;
        }
      });
    }

    if (SubRegion != "") {
      ted = ted.filter((obj) => {
        if (obj.subregion === SubRegion) {
          return obj;
        }
      });
    }

    function comparator(a, b) {
      if (sorting === "official") {
        if (a.name.official < b.name.official) {
          return -1;
        } else if (a.name.official > b.name.official) {
          return 1;
        } else {
          return 0;
        }
      } else if (sorting === "area") {
        if (a.area < b.area) {
          return -1;
        } else if (a.area > b.area) {
          return 1;
        } else {
          return 0;
        }
      } else if (sorting === "population") {
        if (a.population < b.population) {
          return -1;
        } else if (a.population > b.population) {
          return 1;
        } else {
          return 0;
        }
      }
    }
    let sortedData = ted.sort(comparator);

    const append = sortedData.map((obj) => {
      return <Card data={obj} />;
    });
    setScreen(append);
  }

  const handleChange = (event) => {
    let entry = event.target.value;

    setSearchCountry(entry);

    console.log(Region + " " + SubRegion);

    if (Region === "" && SubRegion === "") {
      let filteredCountries = Data.filter((obj) => {
        let uppercaseCountry = obj.name.official.toUpperCase();

        let lowercaseCountry = obj.name.official.toLowerCase();

        if (
          uppercaseCountry.includes(entry) ||
          lowercaseCountry.includes(entry)
        ) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });
      if (filteredCountries.length === 0) {
        const message = <h1>No results to show!!!!</h1>;
        setScreen(message);
      } else {
        setScreen(filteredCountries);
      }
    } else if (Region !== "" && SubRegion !== "") {
      let filteredCountries = Data.filter((obj) => {
        let uppercaseCountry = obj.name.official.toUpperCase();
        let lowercaseCountry = obj.name.official.toLowerCase();
        if (
          (uppercaseCountry.includes(entry) ||
            lowercaseCountry.includes(entry)) &&
          Region === obj.region &&
          SubRegion === obj.subregion
        ) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });

      if (filteredCountries.length === 0) {
        const message = <h1>No results to show!!!!</h1>;
        setScreen(message);
      } else {
        setScreen(filteredCountries);
      }
    } else if (Region !== "" && SubRegion === "") {
      let filteredCountries = Data.filter((obj) => {
        let uppercaseCountry = obj.name.official.toUpperCase();

        let lowercaseCountry = obj.name.official.toLowerCase();
        if (
          (uppercaseCountry.includes(entry) ||
            lowercaseCountry.includes(entry)) &&
          Region === obj.region
        ) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });
      if (filteredCountries.length === 0) {
        const message = <h1>No results to show!!!!</h1>;
        setScreen(message);
      } else {
        setScreen(filteredCountries);
      }
    } else if (Region === "" && SubRegion !== "") {
      let filteredCountries = Data.filter((obj) => {
        let uppercaseCountry = obj.name.official.toUpperCase();

        let lowercaseCountry = obj.name.official.toLowerCase();
        if (
          (uppercaseCountry.includes(entry) ||
            lowercaseCountry.includes(entry)) &&
          SubRegion === obj.subregion
        ) {
          return obj;
        }
      }).map((obj) => {
        return <Card data={obj} />;
      });
      if (filteredCountries.length === 0) {
        const message = <h1>No results to show!!!!</h1>;
        setScreen(message);
      } else {
        setScreen(filteredCountries);
      }
    }
  };

  // Data Handlers

  const allRegions = Data.reduce((accumulator, current) => {
    if (!accumulator.includes(current.region)) {
      accumulator.push(current.region);
    }
    return accumulator;
  }, []);

  const allSubRegions = Data.reduce((accumulator, current) => {
    if (Region !== "") {
      if (
        !accumulator.includes(current.subregion) &&
        current.subregion !== undefined &&
        current.region === Region
      ) {
        accumulator.push(current.subregion);
      }
    } else if (
      !accumulator.includes(current.subregion) &&
      current.subregion !== undefined
    ) {
      accumulator.push(current.subregion);
    }

    return accumulator;
  }, []);

  // JSX handlers

  window.onload = function () {
    const allData = Data.map((obj) => {
      return <Card data={obj} />;
    });

    setScreen(allData);
  };
  const regionOptions = allRegions.map((data) => {
    return <Regioncard data={data} />;
  });

  const subRegionOptions = allSubRegions.map((data) => {
    return <Regioncard data={data} />;
  });

  return (
    <div className="Main">
      <div className="maincontent">
        <div className="searchBar">
          <input
            className="searchCountry"
            placeholder=" Search for a Country.."
            onChange={handleChange}
            name="country"
          ></input>
        </div>

        <div className="filterRegion">
          <select onChange={handleClick}>
            <option disabled selected>
              Filter by Region
            </option>
            {regionOptions}
          </select>
        </div>

        <div className="filterSubregion">
          <select onChange={handleClick1}>
            <option disabled selected>
              Filter by Sub-Region
            </option>
            {subRegionOptions}
          </select>
        </div>

        <div className="sortby">
          <select onChange={handleClick2}>
            <option disabled selected>
              Sort by
            </option>
            <option value="official">Name</option>
            <option value="area">Area</option>
            <option value="population">Population</option>
          </select>
        </div>
      </div>
      <div className="displayWindow">{Screen}</div>
    </div>
  );
}
