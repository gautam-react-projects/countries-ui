import React from "react";

export default function Card(props) {
  const { official } = props.data.name;
  const { population, region, capital, subregion, area } = props.data;
  const { png } = props.data.flags;
  return (
    <div className="card">
      <img className="flag" src={png} />
      <h6>
        <b> Country :{official}</b>{" "}
      </h6>
      <h6>Population : {population} </h6>
      <h6>Capital: {capital} </h6>
      <h6>Region :{region}</h6>
      <h6>Sub-region: {subregion}</h6>
      <h6>Area : {area}</h6>
    </div>
  );
}
